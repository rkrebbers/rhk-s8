<?
$page_title = "just Another Quiz";
// If the form is submitted run the script
if(isset($_POST['submit'])){
$quiz1 = $_POST['quiz1']; 
$quiz2 = $_POST['quiz2'];  

// Check to see if quiz has been completed
if($quiz1 == NULL OR $quiz2 == NULL){
$test_complete .='Please complete the quiz! <a href="javascript:history.go(-1)">Go Back</a>';
}else{
// This will show the correct answer question
if($quiz1 == "4") { 
$test_complete .="Question one is <span class='green'>correct</span>, well done!<br/>";  
}else{ 
$test_complete .="Question one is <span class='red'>incorrect</span>!<br/>"; 
} 
// This will show the correct answer question
if($quiz2 == "4") { 
$test_complete .="Question two is <span class='green'>correct</span>, well done!<br/>"; 
}else{ 
$test_complete .="Question two is <span class='red'>incorrect</span>!<br/>"; 
} 

// This will check to see if the answers are correct
if($quiz1 == "4" & $quiz2 == "4"){
$test_complete .="<p>Well done!</p>"; 
}else{
// Will inform user they have not answered all the questions correctly
$test_complete .='<p>There are incorrect answers! <a href="javascript:history.go(-1)">Have another go!</a></p>'; 
}}}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Simple Script illustrating the use of GIT and PHP</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
<!--
body,td,th {
	color: #000000;
}
.green {color:#009933;}
.red {color:#CC0000;}
-->
</style></head>
<body>
	<? if(!isset($_POST['submit'])){ ?>
	<h2>Just Another Quiz</h2>
	
	<p>This is a simple Quiz script, illustrating the use of GIT and PHP</p>
	
	<form method="post" name="quizform">
	
   
  <p><b>Question 1.</b> The Purpose of GIT is?<br>
    <input type="radio" name="quiz1" value="3">
    Manage a Project<br>
    <input type="radio" name="quiz1" value="2">
	Manage a set of files
    <br>
    <input type="radio" name="quiz1" value="1">
    Manage like a timeline<br>
	<input type="radio" name="quiz1" value="4">
	All of the above<br>
	</p>
  <p><b>Question 2.</b> Database-Driven  Webtechnology is...<br>
    <input type="radio" name="quiz2" value="1">
    Fun<br>
	<input type="radio" name="quiz2" value="5">
	Huh?<br>
    <input type="radio" name="quiz2" value="3">
    Boring<br>
    <input type="radio" name="quiz2" value="2">
    The love of my life<br>
	<input type="radio" name="quiz2" value="4">
	The best lessons i've had in life so far<br>
	</p>
	<p><input type="submit" name="submit" value="Submit"></p>
	
</form>
<? }else{ 
echo "<h2>Quiz Results</h2>
<p>".$test_complete."</p>";
}?>
</body>
</html>
